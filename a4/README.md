> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Brian Conrad

### Assignment 4 Requirements:

1. Create a Bootstrap client-side application that validates data
2. Provide screenshots of home page, error page, and success page
3. Provide a link to localhost
4. Answer chapter questions

#### Assignment Screenshots:

*Screenshot of Home Page*:

![Home Page Screenshot](http://i.imgur.com/UQrgNKm.png "Home Page Screenshot")

*Screenshot of Error Page pt.1*

![Error Page pt.1](http://i.imgur.com/NlqClSV.png "Error Page pt.1")

*Screenshot of Error Page pt.2*

![Error Page pt.2](http://i.imgur.com/XGvL2F0.png "Error Page pt.2")

*Screenshot of Success Page pt.1*

![Success Page pt.1](http://i.imgur.com/TCsxson.png "Success Page pt.1")

*Screenshot of Success Page pt.2*

![Success Page pt.2](http://i.imgur.com/LXrJxFX.png "Success Page pt.2")

#### Assignment Links:

*Local Host Link*

[Local Host Link](http://localhost/repos/a4/index.php "Local Host Link")

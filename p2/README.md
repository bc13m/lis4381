> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Brian Conrad

### Project 2 Requirements:

1. Add a delete and edit function to server-side validation
2. Provide screenshots of error page, data page, and online portfolio
3. Provide a link to localhost
4. Answer chapter questions

#### Assignment Screenshots:

*Screenshot of index.php*:

![Index Page Screenshot](http://i.imgur.com/ZYY8suH.png "Index Page Screenshot")

*Screenshot of Update Page pt.1*

![Update Page pt.1](http://i.imgur.com/mU42y1S.png "Update Page pt.1")

*Screenshot of Update Page pt.2*

![Update Page pt.2](http://i.imgur.com/cGZTnWD.png "Update Page pt.2")

*Screenshot of Error Page*

![Error Page](http://i.imgur.com/7NdIw3G.png "Error Page")

*Screenshot of Carousel*

![Carousel Screenshot](http://i.imgur.com/geBlxio.png "Carousel Screenshot")

*Screenshot of RSS Feed*

![RSS Screenshot](http://i.imgur.com/QYMLd01.png "RSS Screenshot")


*Local Host Link*

[Local Host Link](http://localhost/repos/lis4381/p2/index.php "Local Host Link")

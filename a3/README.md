> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Brian Conrad

### Assignment 3 Requirements:

1. Create a 3 table database in MySQL
2. Provide a screenshot of database
3. Answer chapter questions

#### Assignment Screenshots:

*Screenshot of Database Model*:

![MySQL Database Model Screenshot](http://i.imgur.com/ZPcp7ri.png "MySQL Database Model Screenshot")

#### Assignment Links:

*MySQL .mwb File:*
[MySQL .mwb File Link](https://bitbucket.org/bc13m/lis4381/src/a3e6bdf86b97d75c01470485c99d05c947d8182c/a3/docs/a3model.mwb?at=master&fileviewer=file-view-default ".mwb File")

*MySQL Script File:*
[MySQL .sql File Link](https://bitbucket.org/bc13m/lis4381/src/83f878ea8ef0913aedd2f53f3d989cd202c58919/a3/docs/a3_script.sql?at=master&fileviewer=file-view-default ".sql File")

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Brian Conrad

### LIS4381 Requirements:

1. [A1 README.md](a1/README.md "My A1 README.md file")

    Install AMPPS

    Install JDK

    Install Android Studio and create My First App

    Provide screenshots of installations

    Create Bitbucket repo

    Complete Bitbucket tutorials

    Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

    Created "Bruschetta" app in Android Studio

    Provide screenshot of app

3. [A3 README.md](a3/README.md "My A3 README.md file")

    Create a 3 table database in MySQL

    Provide screenshot of database

4. [A4 README.md](a4/README.md "My A4 README.md file")

    Create a Bootstrap client-side application that validates data

    Provide screenshots of home page, error page, and success page

    Provide a link to localhost

5. [A5 README.md](a5/README.md "My A5 README.md file")

    Create a Bootstrap server-side application that adds and displays data

    Provide screenshots of error page and data page

    Provide a link to localhost

6. [P1 README.md](p1/README.md "My P1 README.md file")

    Create "My Business Card" app in Android Studio

    Add background colors, text shadow to button, borders to both image and button, and add an app icon

    Provide screenshots of both activities 

7. [P2 README.md](p2/README.md "My P2 README.md file")

    Add a delete and edit function to server-side validation

    Provide screenshots of index, update, error, RSS, and carousel pages

    Provide a link to localhost 

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Brian Conrad

### Assignment 5 Requirements:

1. Create a Bootstrap server-side application that adds and displays data
2. Provide screenshots of error page and data page
3. Provide a link to localhost
4. Answer chapter questions

#### Assignment Screenshots:

*Screenshot of Error Page*:

![Error Page Screenshot](http://imgur.com/3uvOyff.png "Error Page Screenshot")

*Screenshot of Data Page pt.1*

![Data Page pt.1](http://imgur.com/idRr0uV.png "Data Page pt.1")

*Screenshot of Data Page pt.2*

![Data Page pt.2](http://imgur.com/2wQW8hz.png "Data Page pt.2")


*Local Host Link*

[Local Host Link](http://localhost/repos/lis4381/a5/index.php "Local Host Link")
